package br.cairu.escola.devs.plenos.managedbeans;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.cairu.escola.devs.plenos.to.AlunoTO;

@ManagedBean(name = "CadastroAlunoBean")
@SessionScoped 
public class CadastroAlunoBean {

	private String cpf = "";
	private String nome = "";
	private String email = "";
	private String telefone = "";
	private LocalDate nascimento;

	/** 
	Fun��o que cadastra o Aluno ap�s clicar no bot�o de "Cadastrar" no formul�rio de inclus�o
	*/
	public void cadastrar() {
		if(validarFormulario()) {
			Date dataNascimento = Date.from(nascimento.atStartOfDay(ZoneId.systemDefault()).toInstant());
			AlunoTO aluno = new AlunoTO();
			aluno.setCpf(cpf);
			aluno.setEmail(email);
			aluno.setNascimento(dataNascimento);
			aluno.setNome(nome);
			aluno.setTelefone(telefone);
			AlunoManagedBean.inserirAluno(aluno);
			limpar();
		} 
	}

	/** 
	Fun��o que valida se os dados inclu�dos n�o estao nulos ou vazios
	*/
	public boolean validarFormulario() {
		if(this.cpf != null && !"".equals(this.cpf)
				&& this.nome != null && !"".equals(this.nome)
				&& this.email != null && !"".equals(this.email)
				&& this.telefone != null && !"".equals(this.telefone)
				&& this.nascimento != null) {
			return true;
		} else {
			return false;
		}
	}

	/** 
	Fun��o que limpa o formul�rio
	*/
	public void limpar() {
		cpf = "";
		nome = "";
		email = "";
		telefone = "";
		nascimento = null;
	}

	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public LocalDate getNascimento() {
		return nascimento;
	}
	public void setNascimento(LocalDate nascimento) {
		this.nascimento = nascimento;
	}

}
