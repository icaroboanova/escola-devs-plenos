package br.cairu.escola.devs.plenos.managedbeans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import br.cairu.escola.devs.plenos.persistence.MatriculaQC;
import br.cairu.escola.devs.plenos.to.MatriculaTO;


@ManagedBean(name = "MatriculaMB")
@SessionScoped 
public class MatriculaManagedBean {
	
	private static MatriculaQC matriculaQC;
	private MenuModel model;
	
	private String texto = "Hello World!";
	private static List<MatriculaTO> matriculas = new ArrayList<MatriculaTO>();
	
	@PostConstruct
    public void init(){
		if(matriculaQC == null) {
			matriculaQC = new MatriculaQC();
		}
		matriculas = matriculaQC.listarMatriculas();
        
        model = new DefaultMenuModel();
    }
	
	public static void inserirMatricula(MatriculaTO matriculaTO) {
		if(matriculaTO != null) {
			matriculaQC.inserirMatricula(matriculaTO);
		}
		recarregar();
	}
	
	public static MatriculaTO consultarMatriculaPorID(String id) {
		return matriculaQC.consultarMatriculaPorId(id);
	}
	
	public static void atualizarMatricula(MatriculaTO matriculaTO) {
		matriculaQC.atualizarMatricula(matriculaTO);
		recarregar();
	}
	
	public static void excluirMatricula(String id) {
		matriculaQC.excluirMatricula(id);
		recarregar();
	}
	
	public static void recarregar() {
		matriculas = matriculaQC.listarMatriculas();
	}
	
	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public List<MatriculaTO> getMatriculas() {
		return matriculas;
	}

	public void setMatriculas(List<MatriculaTO> matriculas) {
		MatriculaManagedBean.matriculas = matriculas;
	}

}