package br.cairu.escola.devs.plenos.persistence;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.transaction.TransactionScoped;
import javax.transaction.Transactional;

import br.cairu.escola.devs.plenos.to.AlunoTO;

@Stateless 
public class AlunoQC {

	private EntityManager entityManager;

	public AlunoQC() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("escolaPU");
		this.entityManager = factory.createEntityManager();
	}

	@SuppressWarnings("unchecked")
	public List<AlunoTO> listarAlunos() {
		List<AlunoTO> retorno = new ArrayList<AlunoTO>();

		final String sql = "select * from alunos";
		Query query = this.entityManager.createNativeQuery(sql);
		List<Object[]> resultados = query.getResultList();
		for(Object[] resultado : resultados) {
			AlunoTO aluno = new AlunoTO();
			aluno.setNome(resultado[2].toString());
			aluno.setCpf(resultado[1].toString());
			aluno.setEmail(resultado[3].toString());
			aluno.setTelefone(resultado[4].toString());
			aluno.setNascimento((Date) resultado[5]);
			aluno.setId(resultado[0].toString());
			retorno.add(aluno);
		}

		return retorno;
	}

	public AlunoTO consultarAlunoPorID(String id) {
		final String sql = "select * from alunos where id = ?";
		Query query = this.entityManager.createNativeQuery(sql)
				.setParameter(1, id);
		Object[] resultado = (Object[]) query.getResultList().get(0);
		AlunoTO aluno = new AlunoTO();
		aluno.setNome(resultado[2].toString());
		aluno.setCpf(resultado[1].toString());
		aluno.setEmail(resultado[3].toString());
		aluno.setTelefone(resultado[4].toString());
		aluno.setNascimento((Date) resultado[5]);
		aluno.setId(resultado[0].toString());
		return aluno;
	}

	@TransactionScoped
	public void inserirAluno(AlunoTO alunoTO) {
		try {
			final String sql = "insert into alunos(cpf, nome, email, fone, data_nascimento)"
					+ " values(?1, ?2, ?3, ?4, ?5)";
			this.entityManager.getTransaction().begin();
			this.entityManager.createNativeQuery(sql).setParameter(1, alunoTO.getCpf())
			.setParameter(2, alunoTO.getNome())
			.setParameter(3, alunoTO.getEmail())
			.setParameter(4, alunoTO.getTelefone())
			.setParameter(5, alunoTO.getNascimento())
			.executeUpdate();
			this.entityManager.getTransaction().commit();
		} catch(Exception e) {
			this.entityManager.getTransaction().rollback();
		}
	}

	@TransactionScoped
	public void atualizarAluno(AlunoTO alunoTO) {
		try {
			final String sql = "update alunos set cpf = ?1, nome = ?2, email = ?3, fone = ?4, "
					+ " data_nascimento = ?5 where id = ?6";
			this.entityManager.getTransaction().begin();
			this.entityManager.createNativeQuery(sql).setParameter(1, alunoTO.getCpf())
			.setParameter(2, alunoTO.getNome())
			.setParameter(3, alunoTO.getEmail())
			.setParameter(4, alunoTO.getTelefone())
			.setParameter(5, alunoTO.getNascimento())
			.setParameter(6, alunoTO.getId())
			.executeUpdate();
			this.entityManager.getTransaction().commit();
		} catch(Exception e) {
			this.entityManager.getTransaction().rollback();
		}
	}

	@TransactionScoped
	public void excluirAluno(String id) {
		try {
			final String sql = "delete from alunos where id = ?";
			this.entityManager.getTransaction().begin();
			this.entityManager.createNativeQuery(sql)
			.setParameter(1, id)
			.executeUpdate();
			this.entityManager.getTransaction().commit();
		} catch(Exception e) {
			this.entityManager.getTransaction().rollback();
		}
	}

}
