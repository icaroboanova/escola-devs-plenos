package br.cairu.escola.devs.plenos.managedbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.cairu.escola.devs.plenos.to.CursoTO;

@ManagedBean(name = "CadastroCursoBean")
@SessionScoped 
public class CadastroCursoBean {
	
	private String id = "";
	private String nome = "";
	private String requisito = "";
	private int cargaHoraria = 0;
	private Double preco = 0.00;
	
	public void cadastrar() {
		if(validarFormulario()) {
			CursoTO curso = new CursoTO();
			curso.setNome(nome);
			curso.setRequisito(requisito);
			curso.setCargaHoraria(cargaHoraria);
			curso.setPreco(preco);
			CursoManagedBean.inserirCurso(curso);
			limpar();
		}
	}
	
	public boolean validarFormulario() {
		if(nome != null && !"".equals(nome)
				&& requisito != null && !"".equals(requisito)
				&& cargaHoraria != 0 && preco != null 
				&& preco != 0.00) {
			return true;
		} else {
			return false;
		}
			
	}
	
	public void limpar() {
		id = "";
		nome = "";
		requisito = "";
		cargaHoraria = 0;
		preco = 0.00;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getRequisito() {
		return requisito;
	}
	public void setRequisito(String requisito) {
		this.requisito = requisito;
	}
	public int getCargaHoraria() {
		return cargaHoraria;
	}
	public void setCargaHoraria(int cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}
	public Double getPreco() {
		return preco;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	
	

}
