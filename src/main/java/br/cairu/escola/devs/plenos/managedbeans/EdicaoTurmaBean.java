package br.cairu.escola.devs.plenos.managedbeans;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.PrimeFaces;

import br.cairu.escola.devs.plenos.to.TurmaTO;


@ManagedBean(name = "EdicaoTurmaBean")
@SessionScoped 
public class EdicaoTurmaBean {
	
	private String id;
	private String idInstrutor;
	private String idCurso;
	private LocalDate dataInicio;
	private LocalDate dataFinal;
	private int cargaHoraria;
	
	public void carregarTurma(String id) {
		if(id != null && !"".equals(id)) {
			TurmaTO turmaTO = TurmaManagedBean.consultarTurmaPorID(id);
			this.id = turmaTO.getId();
			this.idInstrutor = turmaTO.getIdInstrutor();
			this.idCurso = turmaTO.getIdCurso();
			this.cargaHoraria = turmaTO.getCargaHoraria();
			Date inicio = new Date(turmaTO.getDataInicio().getTime());
			Date finalData = new Date(turmaTO.getDataFinal().getTime());
			this.dataInicio = inicio.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			this.dataFinal = finalData.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			PrimeFaces current = PrimeFaces.current();
			current.executeScript("PF('dialog-editar').show();");
		}
	}
	
	public void confirmar() {
			Date inicio = Date.from(dataInicio.atStartOfDay(ZoneId.systemDefault()).toInstant());
			Date finalData = Date.from(dataFinal.atStartOfDay(ZoneId.systemDefault()).toInstant());
			TurmaTO turma = new TurmaTO();
			turma.setId(id);
			turma.setIdInstrutor(idInstrutor);
			turma.setIdCurso(idCurso);
			turma.setDataInicio(inicio);
			turma.setDataFinal(finalData);
			turma.setCargaHoraria(cargaHoraria);
			TurmaManagedBean.atualizarTurma(turma);
			limpar();
	}
	
	public void limpar() {
		idInstrutor = "";
		idCurso = "";
		dataInicio = null;
		dataFinal = null;
		cargaHoraria = 0;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdInstrutor() {
		return idInstrutor;
	}

	public void setIdInstrutor(String idInstrutor) {
		this.idInstrutor = idInstrutor;
	}

	public String getIdCurso() {
		return idCurso;
	}

	public void setIdCurso(String idCurso) {
		this.idCurso = idCurso;
	}

	public LocalDate getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(LocalDate dataInicio) {
		this.dataInicio = dataInicio;
	}

	public LocalDate getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(LocalDate dataFinal) {
		this.dataFinal = dataFinal;
	}

	public int getCargaHoraria() {
		return cargaHoraria;
	}

	public void setCargaHoraria(int cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}
	
}
