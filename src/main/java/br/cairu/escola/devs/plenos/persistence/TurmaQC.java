package br.cairu.escola.devs.plenos.persistence;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.transaction.TransactionScoped;

import br.cairu.escola.devs.plenos.to.TurmaTO;



public class TurmaQC {

	private EntityManager entityManager;

	public TurmaQC() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("escolaPU");
		this.entityManager = factory.createEntityManager();
	}

	@SuppressWarnings("unchecked")
	public List<TurmaTO> listarTurmas() {
		List<TurmaTO> retorno = new ArrayList<TurmaTO>();

		final String sql = "select * from turmas";
		Query query = this.entityManager.createNativeQuery(sql);
		List<Object[]> resultados = query.getResultList();
		for(Object[] resultado : resultados) {
			TurmaTO turma = new TurmaTO();
			turma.setId(resultado[0].toString());
			turma.setIdInstrutor(resultado[1].toString());
			turma.setIdCurso(resultado[2].toString());
			turma.setDataInicio((Date) resultado[3]);
			turma.setDataFinal((Date) resultado[4]);
			turma.setCargaHoraria(Integer.valueOf(resultado[5].toString()));
			retorno.add(turma);
		}

		return retorno;
	}

	public TurmaTO consultarTurmaPorId(String id) {
		final String sql = "select * from turmas where id = ?";
		Query query = this.entityManager.createNativeQuery(sql)
				.setParameter(1, id);
		Object[] resultado = (Object[]) query.getResultList().get(0);
		TurmaTO turma = new TurmaTO();
		turma.setId(resultado[0].toString());
		turma.setIdInstrutor(resultado[1].toString());
		turma.setIdCurso(resultado[2].toString());
		turma.setDataInicio((Date) resultado[3]);
		turma.setDataFinal((Date) resultado[4]);
		turma.setCargaHoraria(Integer.valueOf(resultado[5].toString()));
		return turma;
	}

	public void inserirTurma(TurmaTO turma) {
		try { 
			final String sql = "insert into turmas(instrutores_id, cursos_id, data_inicio, data_final, "
					+ " carga_horaria) values(?1, ?2, ?3, ?4, ?5)";
			this.entityManager.getTransaction().begin();
			this.entityManager.createNativeQuery(sql).setParameter(1, turma.getIdInstrutor())
			.setParameter(2, turma.getIdCurso())
			.setParameter(3, turma.getDataInicio())
			.setParameter(4, turma.getDataFinal())
			.setParameter(5, turma.getCargaHoraria())
			.executeUpdate();
			this.entityManager.getTransaction().commit();
		} catch(Exception e) {
			this.entityManager.getTransaction().rollback();
		}
	}

	public void atualizarTurma(TurmaTO turma) {
		try {
			final String sql = "update turmas set instrutores_id = ?1, cursos_id = ?2, data_inicio = ?3"
					+ ", data_final = ?4, carga_horaria = ?5 where id = ?6";
			this.entityManager.getTransaction().begin();
			this.entityManager.createNativeQuery(sql).setParameter(1, turma.getIdInstrutor())
			.setParameter(2, turma.getIdCurso())
			.setParameter(3, turma.getDataInicio())
			.setParameter(4, turma.getDataFinal())
			.setParameter(5, turma.getCargaHoraria())
			.setParameter(6, turma.getId())
			.executeUpdate();
			this.entityManager.getTransaction().commit();
		} catch(Exception e) {
			this.entityManager.getTransaction().rollback();
		}
	}

	@TransactionScoped
	public void excluirTurma(String id) {
		try {
			final String sql = "delete from turmas where id = ?";
			this.entityManager.getTransaction().begin();
			this.entityManager.createNativeQuery(sql)
			.setParameter(1, id)
			.executeUpdate();
			this.entityManager.getTransaction().commit();
		} catch(Exception e) {
			this.entityManager.getTransaction().rollback();
		}
	}

}
