package br.cairu.escola.devs.plenos.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.transaction.TransactionScoped;

import br.cairu.escola.devs.plenos.to.CursoTO;

public class CursoQC {

	private EntityManager entityManager;

	public CursoQC() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("escolaPU");
		this.entityManager = factory.createEntityManager();
	}

	@SuppressWarnings("unchecked")
	public List<CursoTO> listarCursos() {
		List<CursoTO> retorno = new ArrayList<CursoTO>();

		final String sql = "select * from cursos";
		Query query = this.entityManager.createNativeQuery(sql);
		List<Object[]> resultados = query.getResultList();
		for(Object[] resultado : resultados) {
			CursoTO curso = new CursoTO();
			curso.setId(resultado[0].toString());
			curso.setNome(resultado[1].toString());
			curso.setRequisito(resultado[2].toString());
			curso.setCargaHoraria(Integer.parseInt(resultado[3].toString()));
			curso.setPreco(Double.valueOf(resultado[4].toString()));
			retorno.add(curso);
		}

		return retorno;
	}

	public CursoTO consultarCursoPorId(String id) {
		final String sql = "select * from cursos where id = ?";
		Query query = this.entityManager.createNativeQuery(sql)
				.setParameter(1, id);
		Object[] resultado = (Object[]) query.getResultList().get(0);
		CursoTO curso = new CursoTO();
		curso.setId(resultado[0].toString());
		curso.setNome(resultado[1].toString());
		curso.setRequisito(resultado[2].toString());
		curso.setCargaHoraria(Integer.parseInt(resultado[3].toString()));
		curso.setPreco(Double.valueOf(resultado[4].toString()));
		return curso;
	}

	public void inserirCurso(CursoTO curso) {
		try {
			final String sql = "insert into cursos(nome, requisito, carga_horaria, preco) "
					+ " values(?1, ?2, ?3, ?4)";
			this.entityManager.getTransaction().begin();
			this.entityManager.createNativeQuery(sql).setParameter(1, curso.getNome())
			.setParameter(2, curso.getRequisito())
			.setParameter(3, curso.getCargaHoraria())
			.setParameter(4, curso.getPreco())
			.executeUpdate();
			this.entityManager.getTransaction().commit();
		} catch(Exception e) {
			this.entityManager.getTransaction().rollback();
		}
	}

	public void atualizarCurso(CursoTO curso) {
		try {
			final String sql = "update cursos set nome = ?1, requisito = ?2, carga_horaria = ?3"
					+ ", preco = ?4 where id = ?5";
			this.entityManager.getTransaction().begin();
			this.entityManager.createNativeQuery(sql).setParameter(1, curso.getNome())
			.setParameter(2, curso.getRequisito())
			.setParameter(3, curso.getCargaHoraria())
			.setParameter(4, curso.getPreco())
			.setParameter(5, curso.getId())
			.executeUpdate();
			this.entityManager.getTransaction().commit();
		} catch(Exception e) {
			this.entityManager.getTransaction().rollback();
		}
	}

	@TransactionScoped
	public void excluirCurso(String id) {
		try {
			final String sql = "delete from cursos where id = ?";
			this.entityManager.getTransaction().begin();
			this.entityManager.createNativeQuery(sql)
			.setParameter(1, id)
			.executeUpdate();
			this.entityManager.getTransaction().commit();
		} catch(Exception e) {
			this.entityManager.getTransaction().rollback();
		}
	}

}
