package br.cairu.escola.devs.plenos.managedbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.PrimeFaces;

@ManagedBean(name = "ExclusaoMatriculaBean")
@SessionScoped 
public class ExclusaoMatriculaBean {
	
	private String id;
	private String nome;
	
	public void iniciarExclusao(String id, String nome) {
		if(id != null && !"".equals(id) && nome != null && !"".equals(nome)) {
			this.id = id;
			this.nome = nome;
			PrimeFaces current = PrimeFaces.current();
			current.executeScript("PF('dialog-excluir').show();");
		}
	}
	
	public void confirmar() {
		MatriculaManagedBean.excluirMatricula(id);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
