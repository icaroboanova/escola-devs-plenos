package br.cairu.escola.devs.plenos.managedbeans;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.cairu.escola.devs.plenos.to.TurmaTO;


@ManagedBean(name = "CadastroTurmaBean")
@SessionScoped 
public class CadastroTurmaBean {

	private String id;
	private String idInstrutor;
	private String idCurso;
	private LocalDate dataInicio;
	private LocalDate dataFinal;
	private int cargaHoraria;

	public void cadastrar() {
		Date inicio = Date.from(dataInicio.atStartOfDay(ZoneId.systemDefault()).toInstant());
		Date finalDate = Date.from(dataFinal.atStartOfDay(ZoneId.systemDefault()).toInstant());
		TurmaTO turma = new TurmaTO();
		turma.setIdInstrutor(idInstrutor);
		turma.setIdCurso(idCurso);
		turma.setDataInicio(inicio);
		turma.setDataFinal(finalDate);
		turma.setCargaHoraria(cargaHoraria);
		TurmaManagedBean.inserirTurma(turma);
		limpar();
	}

	public void limpar() {
		id = "";
		idInstrutor = "";
		idCurso = "";
		dataInicio = null;
		dataFinal = null;
		cargaHoraria  = 0;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdInstrutor() {
		return idInstrutor;
	}

	public void setIdInstrutor(String idInstrutor) {
		this.idInstrutor = idInstrutor;
	}

	public String getIdCurso() {
		return idCurso;
	}

	public void setIdCurso(String idCurso) {
		this.idCurso = idCurso;
	}

	public LocalDate getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(LocalDate dataInicio) {
		this.dataInicio = dataInicio;
	}

	public LocalDate getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(LocalDate dataFinal) {
		this.dataFinal = dataFinal;
	}

	public int getCargaHoraria() {
		return cargaHoraria;
	}

	public void setCargaHoraria(int cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}

}
