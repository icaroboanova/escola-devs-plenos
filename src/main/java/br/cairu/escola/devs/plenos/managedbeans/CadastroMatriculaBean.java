package br.cairu.escola.devs.plenos.managedbeans;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.cairu.escola.devs.plenos.to.MatriculaTO;


@ManagedBean(name = "CadastroMatriculaBean")
@SessionScoped 
public class CadastroMatriculaBean {

	private String id;
	private String idTurma;
	private String idAluno;
	private LocalDate dataMatricula;
	

	public void cadastrar() {
		Date matricula = Date.from(dataMatricula.atStartOfDay(ZoneId.systemDefault()).toInstant());
		MatriculaTO matriculaTO = new MatriculaTO();
		matriculaTO.setIdTurma(idTurma);
		matriculaTO.setIdAluno(idAluno);
		matriculaTO.setDataMatricula(matricula);
		MatriculaManagedBean.inserirMatricula(matriculaTO);
		limpar();
	}

	public void limpar() {
		id = "";
		idTurma = "";
		idAluno = "";
		dataMatricula = null;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdTurma() {
		return idTurma;
	}

	public void setIdTurma(String idTurma) {
		this.idTurma = idTurma;
	}

	public String getIdAluno() {
		return idAluno;
	}

	public void setIdAluno(String idAluno) {
		this.idAluno = idAluno;
	}

	public LocalDate getDataMatricula() {
		return dataMatricula;
	}

	public void setDataMatricula(LocalDate dataMatricula) {
		this.dataMatricula = dataMatricula;
	}

}
