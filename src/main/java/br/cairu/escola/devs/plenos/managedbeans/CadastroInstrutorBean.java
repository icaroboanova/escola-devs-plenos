package br.cairu.escola.devs.plenos.managedbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.cairu.escola.devs.plenos.to.CursoTO;
import br.cairu.escola.devs.plenos.to.InstrutorTO;

@ManagedBean(name = "CadastroInstrutorBean")
@SessionScoped 
public class CadastroInstrutorBean {

	private String id = "";
	private String nome = "";
	private String email = "";
	private int valorHora = 0;
	private String certificados = "";

	public void cadastrar() {
		InstrutorTO instrutor = new InstrutorTO();
		instrutor.setNome(nome);
		instrutor.setEmail(email);
		instrutor.setValorHora(valorHora);
		instrutor.setCertificados(certificados);
		InstrutorManagedBean.inserirInstrutor(instrutor);
		limpar();
	}

	public void limpar() {
		id = "";
		nome = "";
		email = "";
		valorHora = 0;
		certificados = "";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getValorHora() {
		return valorHora;
	}

	public void setValorHora(int valorHora) {
		this.valorHora = valorHora;
	}

	public String getCertificados() {
		return certificados;
	}

	public void setCertificados(String certificados) {
		this.certificados = certificados;
	}


}
