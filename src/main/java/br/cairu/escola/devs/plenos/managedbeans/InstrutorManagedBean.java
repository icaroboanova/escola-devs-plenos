package br.cairu.escola.devs.plenos.managedbeans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

import br.cairu.escola.devs.plenos.persistence.InstrutorQC;
import br.cairu.escola.devs.plenos.to.InstrutorTO;


@ManagedBean(name = "InstrutorMB")
@SessionScoped 
public class InstrutorManagedBean {
	
	private static InstrutorQC instrutorQC;
	private MenuModel model;
	private static List<InstrutorTO> instrutores = new ArrayList<InstrutorTO>();
	
	@PostConstruct
    public void init(){
		if(instrutorQC == null) {
			instrutorQC = new InstrutorQC();
		}
		instrutores = instrutorQC.listarInstrutores();
		model = new DefaultMenuModel();
	}
	
	public static void inserirInstrutor(InstrutorTO instrutor) {
		if(instrutor != null) {
			instrutorQC.inserirInstrutor(instrutor);
		}
		recarregar();
	}
	
	public static void atualizarInstrutor(InstrutorTO instrutor) {
		if(instrutor != null) {
			instrutorQC.atualizarInstrutor(instrutor);
		}
		recarregar();
	}
	
	public static InstrutorTO consultarInstrutorPorID(String id) {
		return instrutorQC.consultarInstrutorPorId(id);
	}
	
	public static void excluirInstrutor(String id) {
		instrutorQC.excluirInstrutor(id);
		recarregar();
	}
	
	public static void recarregar() {
		instrutores = instrutorQC.listarInstrutores();
	}

	public List<InstrutorTO> getInstrutores() {
		return instrutores;
	}

	public void setInstrutores(List<InstrutorTO> instrutores) {
		InstrutorManagedBean.instrutores = instrutores;
	}

}
