package br.cairu.escola.devs.plenos.persistence;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.transaction.TransactionScoped;

import br.cairu.escola.devs.plenos.to.MatriculaTO;



public class MatriculaQC {

	private EntityManager entityManager;

	public MatriculaQC() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("escolaPU");
		this.entityManager = factory.createEntityManager();
	}

	@SuppressWarnings("unchecked")
	public List<MatriculaTO> listarMatriculas() {
		List<MatriculaTO> retorno = new ArrayList<MatriculaTO>();

		final String sql = "select * from matriculas";
		Query query = this.entityManager.createNativeQuery(sql);
		List<Object[]> resultados = query.getResultList();
		for(Object[] resultado : resultados) {
			MatriculaTO matricula = new MatriculaTO();
			matricula.setId(resultado[0].toString());
			matricula.setIdTurma(resultado[1].toString());
			matricula.setIdAluno(resultado[2].toString());
			matricula.setDataMatricula((Date) resultado[3]);
			retorno.add(matricula);
		}

		return retorno;
	}

	public MatriculaTO consultarMatriculaPorId(String id) {
		final String sql = "select * from matriculas where id = ?";
		Query query = this.entityManager.createNativeQuery(sql)
				.setParameter(1, id);
		Object[] resultado = (Object[]) query.getResultList().get(0);
		MatriculaTO matricula = new MatriculaTO();
		matricula.setId(resultado[0].toString());
		matricula.setIdTurma(resultado[1].toString());
		matricula.setIdAluno(resultado[2].toString());
		matricula.setDataMatricula((Date) resultado[3]);
		return matricula;
	}

	public void inserirMatricula(MatriculaTO matricula) {
		try {
			final String sql = "insert into matriculas(turmas_id, alunos_id, data_matricula) "
					+ " values(?1, ?2, ?3)";
			this.entityManager.getTransaction().begin();
			this.entityManager.createNativeQuery(sql).setParameter(1, matricula.getIdTurma())
			.setParameter(2, matricula.getIdAluno())
			.setParameter(3, matricula.getDataMatricula())
			.executeUpdate();
			this.entityManager.getTransaction().commit();
		} catch(Exception e) {
			this.entityManager.getTransaction().rollback();
		}
	}

	public void atualizarMatricula(MatriculaTO matricula) {
		try {
			final String sql = "update matriculas set turmas_id = ?1, alunos_id = ?2, data_matricula = ?3"
					+ " where id = ?4";
			this.entityManager.getTransaction().begin();
			this.entityManager.createNativeQuery(sql).setParameter(1, matricula.getIdTurma())
			.setParameter(2, matricula.getIdAluno())
			.setParameter(3, matricula.getDataMatricula())
			.setParameter(4, matricula.getId())
			.executeUpdate();
			this.entityManager.getTransaction().commit();
		} catch(Exception e) {
			this.entityManager.getTransaction().rollback();
		}
	}

	@TransactionScoped
	public void excluirMatricula(String id) {
		try {
			final String sql = "delete from matriculas where id = ?";
			this.entityManager.getTransaction().begin();
			this.entityManager.createNativeQuery(sql)
			.setParameter(1, id)
			.executeUpdate();
			this.entityManager.getTransaction().commit();
		} catch(Exception e) {
			this.entityManager.getTransaction().rollback();
		}
	}

}
