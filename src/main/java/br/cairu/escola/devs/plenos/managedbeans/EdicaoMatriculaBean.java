package br.cairu.escola.devs.plenos.managedbeans;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.PrimeFaces;

import br.cairu.escola.devs.plenos.to.MatriculaTO;

@ManagedBean(name = "EdicaoMatriculaBean")
@SessionScoped 
public class EdicaoMatriculaBean {
	
	private String id;
	private String idTurma;
	private String idAluno;
	private LocalDate matricula;
	
	public void carregarMatricula(String id) {
		if(id != null && !"".equals(id)) {
			MatriculaTO matriculaTO = MatriculaManagedBean.consultarMatriculaPorID(id);
			id = matriculaTO.getId();
			idTurma = matriculaTO.getIdTurma();
			idAluno = matriculaTO.getIdAluno();
			Date dataNascimento = new Date(matriculaTO.getDataMatricula().getTime());
			matricula = dataNascimento.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			PrimeFaces current = PrimeFaces.current();
			current.executeScript("PF('dialog-editar').show();");
		}
	}
	
	public void confirmar() {
			Date dataMatricula = Date.from(matricula.atStartOfDay(ZoneId.systemDefault()).toInstant());
			MatriculaTO matricula = new MatriculaTO();
			matricula.setId(id);
			matricula.setIdTurma(idTurma);
			matricula.setIdAluno(idAluno);
			matricula.setDataMatricula(dataMatricula);
			MatriculaManagedBean.atualizarMatricula(matricula);
			limpar();
	}
	
	public void limpar() {
		idTurma = "";
		idAluno = "";
		matricula = null;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdTurma() {
		return idTurma;
	}

	public void setIdTurma(String idTurma) {
		this.idTurma = idTurma;
	}

	public String getIdAluno() {
		return idAluno;
	}

	public void setIdAluno(String idAluno) {
		this.idAluno = idAluno;
	}

	public LocalDate getDataMatricula() {
		return matricula;
	}

	public void setDataMatricula(LocalDate dataMatricula) {
		this.matricula = dataMatricula;
	}
	
}
