package br.cairu.escola.devs.plenos.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.transaction.TransactionScoped;

import br.cairu.escola.devs.plenos.to.InstrutorTO;


public class InstrutorQC {

	private EntityManager entityManager;

	public InstrutorQC() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("escolaPU");
		this.entityManager = factory.createEntityManager();
	}

	@SuppressWarnings("unchecked")
	public List<InstrutorTO> listarInstrutores() {
		List<InstrutorTO> retorno = new ArrayList<InstrutorTO>();

		final String sql = "select * from instrutores";
		Query query = this.entityManager.createNativeQuery(sql);
		List<Object[]> resultados = query.getResultList();
		for(Object[] resultado : resultados) {
			InstrutorTO instrutor = new InstrutorTO();
			instrutor.setId(resultado[0].toString());
			instrutor.setNome(resultado[1].toString());
			instrutor.setEmail(resultado[2].toString());
			instrutor.setValorHora(Integer.parseInt(resultado[3].toString()));
			instrutor.setCertificados(resultado[4].toString());
			retorno.add(instrutor);
		}

		return retorno;
	}

	public InstrutorTO consultarInstrutorPorId(String id) {
		final String sql = "select * from instrutores where id = ?";
		Query query = this.entityManager.createNativeQuery(sql)
				.setParameter(1, id);
		Object[] resultado = (Object[]) query.getResultList().get(0);
		InstrutorTO instrutor = new InstrutorTO();
		instrutor.setId(resultado[0].toString());
		instrutor.setNome(resultado[1].toString());
		instrutor.setEmail(resultado[2].toString());
		instrutor.setValorHora(Integer.parseInt(resultado[3].toString()));
		instrutor.setCertificados(resultado[4].toString());
		return instrutor;
	}

	public void inserirInstrutor(InstrutorTO instrutor) {
		try {
			final String sql = "insert into instrutores(nome, email, valor_hora, certificados) "
					+ " values(?1, ?2, ?3, ?4)";
			this.entityManager.getTransaction().begin();
			this.entityManager.createNativeQuery(sql).setParameter(1, instrutor.getNome())
			.setParameter(2, instrutor.getEmail())
			.setParameter(3, instrutor.getValorHora())
			.setParameter(4, instrutor.getCertificados())
			.executeUpdate();
			this.entityManager.getTransaction().commit();
		} catch(Exception e) {
			this.entityManager.getTransaction().rollback();
		}
	}

	public void atualizarInstrutor(InstrutorTO instrutor) {
		try {
			final String sql = "update instrutores set nome = ?1, email = ?2, valor_hora = ?3"
					+ ", certificados = ?4 where id = ?5";
			this.entityManager.getTransaction().begin();
			this.entityManager.createNativeQuery(sql).setParameter(1, instrutor.getNome())
			.setParameter(2, instrutor.getEmail())
			.setParameter(3, instrutor.getValorHora())
			.setParameter(4, instrutor.getCertificados())
			.setParameter(5, instrutor.getId())
			.executeUpdate();
			this.entityManager.getTransaction().commit();
		} catch(Exception e) {
			this.entityManager.getTransaction().rollback();
		}
	}

	@TransactionScoped
	public void excluirInstrutor(String id) {
		try {
			final String sql = "delete from instrutores where id = ?";
			this.entityManager.getTransaction().begin();
			this.entityManager.createNativeQuery(sql)
			.setParameter(1, id)
			.executeUpdate();
			this.entityManager.getTransaction().commit();
		} catch(Exception e) {
			this.entityManager.getTransaction().rollback();
		}
	}

}
