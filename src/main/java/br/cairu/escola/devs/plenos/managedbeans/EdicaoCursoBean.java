package br.cairu.escola.devs.plenos.managedbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.PrimeFaces;

import br.cairu.escola.devs.plenos.to.CursoTO;

@ManagedBean(name = "EdicaoCursoBean")
@SessionScoped 
public class EdicaoCursoBean {

	private String id = "";
	private String nome = "";
	private String requisito = "";
	private int cargaHoraria = 0;
	private Double preco = 0.00;
	
	public void carregarCurso(String id) {
		if(id != null && !"".equals(id)) {
			CursoTO curso = CursoManagedBean.consultarCursoPorID(id);
			this.id = curso.getId();
			this.nome = curso.getNome();
			this.requisito = curso.getRequisito();
			this.cargaHoraria = curso.getCargaHoraria();
			this.preco = curso.getPreco();
			PrimeFaces current = PrimeFaces.current();
			current.executeScript("PF('dialog-editar').show();");
		}
	}
	
	public void confirmar() {
			CursoTO curso = new CursoTO();
			curso.setId(id);
			curso.setNome(nome);
			curso.setRequisito(requisito);
			curso.setCargaHoraria(cargaHoraria);
			curso.setPreco(preco);
			CursoManagedBean.atualizarCurso(curso);
			limpar();
	}
	
	public void limpar() {
		id = "";
		nome = "";
		requisito = "";
		cargaHoraria = 0;
		preco = 0.00;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getRequisito() {
		return requisito;
	}
	public void setRequisito(String requisito) {
		this.requisito = requisito;
	}
	public int getCargaHoraria() {
		return cargaHoraria;
	}
	public void setCargaHoraria(int cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}
	public Double getPreco() {
		return preco;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	
}
