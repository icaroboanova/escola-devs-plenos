package br.cairu.escola.devs.plenos.managedbeans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import br.cairu.escola.devs.plenos.persistence.TurmaQC;
import br.cairu.escola.devs.plenos.to.TurmaTO;



@ManagedBean(name = "TurmaMB")
@SessionScoped 
public class TurmaManagedBean {
	
	private static TurmaQC turmaQC;
	private MenuModel model;
	
	private String texto = "Hello World!";
	private static List<TurmaTO> turmas = new ArrayList<TurmaTO>();
	
	@PostConstruct
    public void init(){
		if(turmaQC == null) {
			turmaQC = new TurmaQC();
		}
		turmas = turmaQC.listarTurmas();
        
        model = new DefaultMenuModel();
    }
	
	public static void inserirTurma(TurmaTO turmaTO) {
		if(turmaTO != null) {
			turmaQC.inserirTurma(turmaTO);
		}
		recarregar();
	}
	
	public static TurmaTO consultarTurmaPorID(String id) {
		return turmaQC.consultarTurmaPorId(id);
	}
	
	public static void atualizarTurma(TurmaTO turmaTO) {
		turmaQC.atualizarTurma(turmaTO);
		recarregar();
	}
	
	public static void excluirTurma(String id) {
		turmaQC.excluirTurma(id);
		recarregar();
	}
	
	public static void recarregar() {
		turmas = turmaQC.listarTurmas();
	}
	
	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public List<TurmaTO> getTurmas() {
		return turmas;
	}

	public void setTurmas(List<TurmaTO> turmas) {
		TurmaManagedBean.turmas = turmas;
	}

}