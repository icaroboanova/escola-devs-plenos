package br.cairu.escola.devs.plenos.managedbeans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

import br.cairu.escola.devs.plenos.persistence.CursoQC;
import br.cairu.escola.devs.plenos.to.CursoTO;

@ManagedBean(name = "CursoMB")
@SessionScoped 
public class CursoManagedBean {
	
	private static CursoQC cursoQC;
	private MenuModel model;
	private static List<CursoTO> cursos = new ArrayList<CursoTO>(); 
	
	@PostConstruct
    public void init(){
		if(cursoQC == null) {
			cursoQC = new CursoQC();
		}
		cursos = cursoQC.listarCursos();
		model = new DefaultMenuModel();
	}
	
	public static void inserirCurso(CursoTO curso) {
		if(curso != null) {
			cursoQC.inserirCurso(curso);
		}
		recarregar();
	}
	
	public static void atualizarCurso(CursoTO curso) {
		if(curso != null) {
			cursoQC.atualizarCurso(curso);
		}
		recarregar();
	}
	
	public static CursoTO consultarCursoPorID(String id) {
		return cursoQC.consultarCursoPorId(id);
	}
	
	public static void excluirCurso(String id) {
		cursoQC.excluirCurso(id);
		recarregar();
	}
	
	public static void recarregar() {
		cursos = cursoQC.listarCursos();
	}

	public List<CursoTO> getCursos() {
		return cursos;
	}

	public void setCursos(List<CursoTO> cursos) {
		CursoManagedBean.cursos = cursos;
	}

}
