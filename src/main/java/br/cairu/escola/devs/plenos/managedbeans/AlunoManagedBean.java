package br.cairu.escola.devs.plenos.managedbeans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import br.cairu.escola.devs.plenos.persistence.AlunoQC;
import br.cairu.escola.devs.plenos.to.AlunoTO;

@ManagedBean(name = "AlunoMB")
@SessionScoped 
public class AlunoManagedBean {
	
	private static AlunoQC alunoQC;
	private MenuModel model;
	
	private String texto = "Hello World!";
	private static List<AlunoTO> alunos = new ArrayList<AlunoTO>();
	
	/** 
	Inicia o QueryComponent e lista todos os alunos
	*/
	@PostConstruct
    public void init(){
		if(alunoQC == null) {
			alunoQC = new AlunoQC();
		}
        alunos = alunoQC.listarAlunos();
        
        model = new DefaultMenuModel();
    }
	
	/** 
	Fun��o que chama o QC para inserir um aluno e � chamada pelo Bean de Cadastro
	*/
	public static void inserirAluno(AlunoTO alunoTO) {
		if(alunoTO != null) {
			alunoQC.inserirAluno(alunoTO);
		}
		recarregar();
	}
	
	public static AlunoTO consultarAlunoPorID(String id) {
		return alunoQC.consultarAlunoPorID(id);
	}
	
	public static void atualizarAluno(AlunoTO alunoTO) {
		alunoQC.atualizarAluno(alunoTO);
		recarregar();
	}
	
	public static void excluirAluno(String id) {
		alunoQC.excluirAluno(id);
		recarregar();
	}
	
	public static void recarregar() {
		alunos = alunoQC.listarAlunos();
	}
	
	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public List<AlunoTO> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<AlunoTO> alunos) {
		this.alunos = alunos;
	}
	
}