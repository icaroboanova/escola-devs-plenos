package br.cairu.escola.devs.plenos.managedbeans;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.PrimeFaces;

import br.cairu.escola.devs.plenos.to.AlunoTO;

@ManagedBean(name = "EdicaoAlunoBean")
@SessionScoped 
public class EdicaoAlunoBean {
	
	private String id;
	private String cpf;
	private String nome;
	private String email;
	private String telefone;
	private LocalDate nascimento;
	
	public void carregarAluno(String id) {
		if(id != null && !"".equals(id)) {
			AlunoTO alunoTO = AlunoManagedBean.consultarAlunoPorID(id);
			this.id = alunoTO.getId();
			this.cpf = alunoTO.getCpf();
			this.nome = alunoTO.getNome();
			this.email = alunoTO.getEmail();
			this.telefone = alunoTO.getTelefone();
			Date dataNascimento = new Date(alunoTO.getNascimento().getTime());
			this.nascimento = dataNascimento.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			PrimeFaces current = PrimeFaces.current();
			current.executeScript("PF('dialog-editar').show();");
		}
	}
	
	public void confirmar() {
		if(validarFormulario()) {
			Date dataNascimento = Date.from(nascimento.atStartOfDay(ZoneId.systemDefault()).toInstant());
			AlunoTO aluno = new AlunoTO();
			aluno.setId(id);
			aluno.setCpf(cpf);
			aluno.setEmail(email);
			aluno.setNascimento(dataNascimento);
			aluno.setNome(nome);
			aluno.setTelefone(telefone);
			AlunoManagedBean.atualizarAluno(aluno);
			limpar();
		}
	}
	
	public void limpar() {
		cpf = "";
		nome = "";
		email = "";
		telefone = "";
		nascimento = null;
	}
	
	public boolean validarFormulario() {
		if(this.cpf != null && !"".equals(this.cpf)
				&& this.nome != null && !"".equals(this.nome)
				&& this.email != null && !"".equals(this.email)
				&& this.telefone != null && !"".equals(this.telefone)
				&& this.nascimento != null) {
			return true;
		} else {
			return false;
		}
	}
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public LocalDate getNascimento() {
		return nascimento;
	}
	public void setNascimento(LocalDate nascimento) {
		this.nascimento = nascimento;
	}

}
