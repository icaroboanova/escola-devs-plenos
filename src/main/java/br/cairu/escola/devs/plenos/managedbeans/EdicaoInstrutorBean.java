package br.cairu.escola.devs.plenos.managedbeans;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.PrimeFaces;

import br.cairu.escola.devs.plenos.to.AlunoTO;
import br.cairu.escola.devs.plenos.to.InstrutorTO;

@ManagedBean(name = "EdicaoInstrutorBean")
@SessionScoped 
public class EdicaoInstrutorBean {
	
	private String id;
	private String nome;
	private String email;
	private int valorHora;
	private String certificados;
	
	public void carregarInstrutor(String id) {
		if(id != null && !"".equals(id)) {
			InstrutorTO instrutor = InstrutorManagedBean.consultarInstrutorPorID(id);
			this.id = instrutor.getId();
			this.nome = instrutor.getNome();
			this.email = instrutor.getEmail();
			this.valorHora = instrutor.getValorHora();
			this.certificados = instrutor.getCertificados();
			PrimeFaces current = PrimeFaces.current();
			current.executeScript("PF('dialog-editar').show();");
		}
	}
	
	public void confirmar() {
			InstrutorTO instrutor = new InstrutorTO();
			instrutor.setId(id);
			instrutor.setNome(nome);
			instrutor.setEmail(email);
			instrutor.setValorHora(valorHora);
			instrutor.setCertificados(certificados);
			InstrutorManagedBean.atualizarInstrutor(instrutor);
			limpar();
	}
	
	public void limpar() {
		id = "";
		nome = "";
		email = "";
		valorHora = 0;
		certificados = "";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getValorHora() {
		return valorHora;
	}

	public void setValorHora(int valorHora) {
		this.valorHora = valorHora;
	}

	public String getCertificados() {
		return certificados;
	}

	public void setCertificados(String certificados) {
		this.certificados = certificados;
	}
	
}
